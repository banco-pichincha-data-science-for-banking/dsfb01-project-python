# Configuración Inicial

## Versionamiento

Para el versionamiento se utilizará Git, el cual es un Sistema de Control de Versiones que permite gestionar ágilmente los cambios en los códigos de programación y poder revertirlos de forma distribuida. Se eligió la plataforma GitLab, porque nos permite contar con más de 10 colaboradores en un repositorio privado gratuito.

1. Instalar Git desde su página oficial: [Git](https://git-scm.com/)
2. Crear una cuenta de usuario en GitLab: [GitLab](https://gitlab.com/users/sign_in)
3. Descargar GitHub Desktop: [GitHub Desktop](https://desktop.github.com/)

Configurar la conexión entre el repositorio en GitLab y la computadora local. 

¿Cómo funciona Git?: [Opción 1](https://www.youtube.com/playlist?list=PLCTD_CpMeEKTnfAeWT3C3PFAKf4abEOpw), [Opción 2](https://reviblog.net/2018/03/29/tutorial-de-git-aprende-git-y-github-gitlab-de-manera-facil-rapida-y-sencilla-parte-1/)
¿Cuál elijo, GitHub o GitLab? [Artículo](https://www.redeszone.net/2019/01/10/github-vs-gitlab-diferencias/)

Clonar un repositorio en [Google Colaboratory](https://colab.research.google.com/notebooks/welcome.ipynb#recent=true)

```python
!git clone https://<usuario>:<Password>@gitlab.com/banco-pichincha-data-science-for-banking/dsfb01-project-python.git
```

## Ordenamiento

Es importante mantener el orden en un proyecto. Si es posible, se debe documentar los resultados mientras avanzamos.

Primero configuramos las carpetas básicas que nos deben de acompañar durante todo el proyecto.

> * **code**: scripts y códigos
> * **document**: documentación detallada
> * **result**: resultados obtenidos
> * **src**: fuentes utilizadas

## Plus
Tunear Jupyter Notebook: [extensiones y temas](https://towardsdatascience.com/bringing-the-best-out-of-jupyter-notebooks-for-data-science-f0871519ca29)