# Exploración y tratamiento

El proceso genera un bucle que continua hasta encontrar estabilidad con el objetivo de perfilar los datos que finalmente serán utilizados en la construcción del modelo.

## Análisis univariante y bivariante
Analisis exploratorio a través de gráfico, tablas de frecuencia, y/o indicadores estadísticos el comportamiento de las variables que ingresarán al modelo.
En el univariante, se analiza la distribución variable por variable, mientras que en el bivariado, se analiza cada una de las variables con el target.

## Tratamiento de variables
Tiene como objetivo mejorar la calidad de los datos que serán utilizados en la construcción del modelo.

### Tratamiento de Missings y Outliers
Algunas técnicas:

* Eliminación de registros
* Sustitución por otro valor
* Inclusión de dummies
* Discretización de variables

### Mejoramiento en la tendencia de las variables
Algunas técnicas:

* Tratamiento del Target
* Suavización de variables
* Discretización

### Discretización de variables
Creación de nuevas variables a partir de variables continuas, mediante la creación de grupos homogeneos:

* Creación de grupos de forma experta a partir de análisis de frecuencias o histogramas 
* Métodos de clustering
* Discretización automática buscando heterogeneidad del target

### Transformación de variables
Creación de nuevas variables a partir de otras existentes:

* Escalamiento
* Transformar fechas en antigüedad
* Crear ratios, promedios, conteos, máximos, mínimos, etc.

Cargar librerias
```python
import pandas as pd
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as sc 
```

Cargar datos a un dataframe de Pandas

```python
ruta = '../../src/db/'
fileAB = 'fraudemovilBEBOOO.csv' 
path = os.path.join(ruta,fileAB)
telco_fraude = pd.read_csv(path,sep=';')
```
Exploración rápida
```python
telco_fraude.shape
telco_fraude.columns
telco_fraude.dtypes
telco_fraude.describe()
```

Importancia del [tipo de datos](https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/tipos.html#clasificacion): int, float, bool, str, category, entre otros...
Es posible definir otros parámetros al momento de cargar los datos como `nrow`, `dtype`, `select`, entre otros.

```python
tipo_datos = {'PRODUCTO':'category','DESPLAN':'category','DEPARTAMENTO':'category','PROVINCIA':'str','DISTRITO':'str','ESTANEXO':'category','FA_TELF_ANO':'category','FA_TELF_MES':'category','FA_TELF_DIA':'category','FA_CLI_ANO':'category','FA_CLI_MES':'category','FA_CLI_DIA':'category','CODPLAN':'category','TARGET':'category'}

telco_fraude = pd.read_csv(path,sep=';',dtype=tipo_datos)
```

Verificar los nulos en la data
```python
for i in telco_fraude.columns:
    print(i,": ",telco_fraude[i].isna().sum())
```


```python
telco_fraude.PROVINCIA[telco_fraude.PROVINCIA.isna()]="VACIO"
telco_fraude.DISTRITO[telco_fraude.DISTRITO.isna()]="VACIO"

telco_fraude.PROVINCIA=telco_fraude.PROVINCIA.astype('category')
telco_fraude.DISTRITO=telco_fraude.DISTRITO.astype('category')
```

Identificando a la descripción del plan null
```python
telco_fraude[telco_fraude.CODPLAN=='1623']
telco_fraude[telco_fraude.DESPLAN.isna()]
```

```python
df=telco_fraude
for i in range(len(meta)) :
    v=meta.iloc[i].variable 
    t=meta.iloc[i].tipo
    if (t.__class__.__name__=="CategoricalDtype"):
        x=pd.DataFrame({"var":v,"mean": ".",
                        "median": ".",
                        "mode": ".",
                        "min": ".",
                        "max": ".",
                        "sd": ".",
                        "cv": ".",
                        "k": ".",
                        "Q1": ".",
                        "Q3": ".",
                        "Nmiss": "."
                        },index=[i])
    else:
        P25=np.percentile(df[v],q=25)
        P75=np.percentile(df[v],q=75)
        IQR=P75-P25
        liV=P25-1.5*IQR # Mimimo Viable
        lsV=P75+1.5*IQR # Maximo Viable
        x=pd.DataFrame({"var":v,"mean": df[v].mean(),
                        "median": df[v].median(),
                        "mode": df[v].mode(),
                        "min": df[v].min(),
                        "max": df[v].max(),
                        "sd": df[v].std(),
                        "cv": df[v].std()/df[v].mean(),
                        "k": sc.kurtosis(df[v]),
                        "Q1": np.percentile(df[v],q=25),
                        "Q3": np.percentile(df[v],q=75),
                        "Nmiss": df[v].isnull().sum()/len(df)
                        }, index=[i])
    if(i==0):
        x1=x
    else:
        x1=pd.concat([x1, x])  # x1.append(x)
```
## Análisis univariante y bivariante
Analisis exploratorio a través de gráfico, tablas de frecuencia, y/o indicadores estadísticos el comportamiento de las variables que ingresarán al modelo.
En el univariante, se analiza la distribución variable por variable, mientras que en el bivariado, se analiza cada una de las variables con el target.


### Automatización de [gráficos](https://towardsdatascience.com/matplotlib-tutorial-learn-basics-of-pythons-powerful-plotting-library-b5d1b8f67596)

![Como funciona matplotlib](https://files.realpython.com/media/fig_map.bc8c7cabd823.png) ![Elementos de un gráfico](https://files.realpython.com/media/anatomy.7d033ebbfbc8.png)

```python
v=pd.DataFrame({"variable": telco_fraude.columns.values})
t=pd.DataFrame({"tipo": telco_fraude.dtypes.values})
meta = pd.concat([v, t], axis=1)
```

Análisis univariante
```python
ds1=telco_fraude
matplotlib.rcParams.update({'font.size': 16})
for i in range(len(meta)) :
    plt.figure(figsize=(10,5))
    v=meta.iloc[i].variable 
    t=meta.iloc[i].tipo
    if (t.__class__.__name__=="CategoricalDtype"):
        fa=ds1[v].value_counts() 
        fr=fa/len(ds1[v]) 
        #Barras
        plt.subplot(1,2,1)
        plt.bar(fa.index,fa)
        plt.xticks(fa.index)
        plt.title(v)
        #Pie
        plt.subplot(1,2,2)
        plt.pie(fr,autopct='%1.1f%%', shadow=True, startangle=90)
        plt.legend(fr.index,loc="center left",bbox_to_anchor=(1, 0, 0.5, 1))
        plt.title(v)
        #Guardar
        plt.savefig("../../result/01prep/univariado/"+ i + v + ".jpg")
    else:
        #Histograma
        plt.subplot(1,2,1)
        plt.hist(ds1[v])
        plt.title(v)
        #Boxplot
        plt.subplot(1,2,2)
        plt.boxplot(ds1[v])
        plt.title(v)
        #Guardar
        plt.savefig("../../result/01prep/univariado/"+ i + v + ".jpg")
    plt.show()
```

Análisis bivariante

```python
ds1=telco_fraude
matplotlib.rcParams.update({'font.size': 16})
for i in range(len(meta)) :
    plt.figure(figsize=(10,5))
    v=meta.iloc[i].variable 
    t=meta.iloc[i].tipo
    if (t.__class__.__name__=="CategoricalDtype"):
        fa=ds1[v].value_counts() 
        fr=fa/len(ds1[v]) 
        #Barras
        plt.subplot(1,2,1)
        plt.bar(fa.index,fa)
        plt.xticks(fa.index)
        plt.title(v)
        #Pie
        plt.subplot(1,2,2)
        plt.pie(fr,autopct='%1.1f%%', shadow=True, startangle=90)
        plt.legend(fr.index,loc="center left",bbox_to_anchor=(1, 0, 0.5, 1))
        plt.title(v)
        #Guardar
        plt.savefig("../../result/01prep/univariado/"+ i + v + ".jpg")
    else:
        #Histograma
        plt.subplot(1,2,1)
        plt.hist(ds1[v])
        plt.title(v)
        #Boxplot
        plt.subplot(1,2,2)
        plt.boxplot(ds1[v])
        plt.title(v)
        #Guardar
        plt.savefig("../../result/01prep/univariado/"+ i + v + ".jpg")
    plt.show()
```


## 1. tratamiento de missings
```python
from sklearn.preprocessing import Imputer

Variables_imputer = ['SAL_OFFNET_NLLAM','SAL_OFFNET_NDESTDIF','SAL_ONNETRPB_NLLAM']


imp=Imputer(missing_values='NaN',strategy='median',axis=0)
imp=imp.fit(telco_fraude[Variables_imputer])
telco_fraude[Variables_imputer]=imp.transform(telco_fraude[Variables_imputer])

for i in telco_fraude[Variables_imputer].columns:
    print(i,": ",telco_fraude[i].isna().sum())
```

# 2. tratamiento de colas
```python
P95=np.percentile(telco_fraude['ANT_CLIENTE'],q=95) # P95 :117.65
P95
X=telco_fraude['ANT_CLIENTE']
for i in range(len(X)):
    if(X[i]>P95): X[i]=P95
#X.hist()
plt.boxplot(X)
```
# 3. Codificacion de variables categoricas
```python
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

lblEnc=LabelEncoder()
X=telco_fraude['PRODUCTO'] # Variables independientes (Predictoras)
X=lblEnc.fit_transform(X)
X
```

# 4. Escalamiento de Variables
```python
from sklearn.preprocessing import StandardScaler
scaler=StandardScaler().fit(telco_fraude[Variables_imputer])
scaler

scaler.mean_ 
scaler.scale_ 
scaler.transform(telco_fraude[Variables_imputer]) 

#scaler.transform(telco_fraude_test) Es posible guardar los resultados para aplicarlos en el test.
```

# 5. Separacion de muestras en Train y Test
```python
variables_modelo = ['SAL_TOTAL_NLLAM', 'SAL_TOTAL_NDESTDIF', 'SLMM_LLAM_ONNET_0_30','ENT_TOTAL_MIN', 'ENT_TOTAL_NLLAM']
X = telco_fraude.TARGET
y = telco_fraude[variables_modelo]

from sklearn.model_selection import train_test_split
X_train, X_test,y_train, y_test =train_test_split(X,y,test_size = 0.2,random_state = 0)
```
