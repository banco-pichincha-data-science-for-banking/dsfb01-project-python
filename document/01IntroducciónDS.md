#  DATA SCIENCE

<div style="text-align: justify ">  
El Data Science en la ciencia que estudia la extracción de conocimiento a partir de los datos. Es un procedimiento por el que se obtiene información muy significativa de los datos extraídos. Este tipo de ciencia de los datos  está apareciendo a causa de la necesidad de trabajar con conjuntos desmedidos de datos (conocidos como Big Data), que están formados por datos estructurados como por datos semiestructurados o desestructurados. Todo este conjunto de datos está proporcionado por la información que generan los dispositivos electrónicos, las redes sociales y la web.
</div>



<img src="../src/img/TendTecno.JPG" width="480" height="480" align="center"/>

# 1. Datos: 

### 1.1 Data Warehouse:

Es una colección de datos orientada, integrada, cronológico y no volátil para el soporte de decisiones de negocio.

+ **Orientada**   : Bajo reglas de negocio: Ventas, Productos, clientes, Campañas de Marketing, etc
+ **Integrada**   : Se concentra tanto la información operacional de la empresa y las fuentes de datos externas que puedan existir
+ **Cronológico** : Toda la información del data warehouse tiene un espacio temporal acuñado. 
+ **No Volátil**  : Una vez cargada en el data warehouse, esta no es modificada. 


<img src="../src/img/DataWarehouse.JPG" align="center"/>

### 1.2 Data Lake

Un data lake es un repositorio de almacenamiento que contiene una gran cantidad de datos en bruto (datos estructurados y no estructurados) y que se mantiene allí hasta que sea necesario. 

<img src="../src/img/DataLake.JPG" width="480" height="480" align="center"/>

# 2. INTELIGENCIA ARTIFICIAL

+ **Inteligencia Artificial**: Técnica que permite a las maquinas imitar el comportamiento humano
+ **Machine Learning** : Conjunto de técnicas de la inteligencia artificial que ultilizan metodos estadísticos para permitir a las maquinas mejorar a travez de la experiencia
+ **Deep Learning**: Es un subconjunto de Machine Learning que permite realizar el cálculo computacional de redes neuronales multicapas 

<img src="../src/img/AI_ML_DL.png" width="360" height="360"  align="center"/>

# 2.1 Machine Learning

<div style="text-align: justify ">  
Es un campo de las ciencias de la computación que se encarga de “aprender” dado un conjunto de datos. En otras palabras, se encarga de representar la estructura y generalizar comportamientos de los datos dados.
Día a día se almacenan gigantescas cantidades de datos. El avance de la tecnología hace que se abaraten los costos de almacenamiento de información. La información que se almacena no representa mayores costos y se almacenan con la esperanza de analizarlos más adelante.
</div>

<img src="../src/img/MLDEF.JPG" width="360" height="360"  align="center"/>

<h1><center>“La máquina no aprende por sí misma, sino un algoritmo de su programación que se modifica con la constante entrada de datos para poder predecir eventos”
</center></h1>



Fuente: (https://bit.ly/1FDMTJj)

-Universidad de Stanford

# 3. FUNCIONES Y TAREAS EN MACHINE LEARNING

**Clasificación:** 
+ Clasificar si un cliente desea adquirir una tarjeta de crédito o un producto especifico. 
+ Se pueden clasificar en más de dos niveles.

**Estimación:** 
+ Predecir un valor no conocido y que sea de carácter continuo. Ejemplo: Un estimador de Ingresos. 

**Predicción**
+ Estrictamente es la tarea de identificar un valor en el futuro. 
+ Demanda de un producto, Estimación de precios. Estimación del valor de una acción en el mercado. 

**Clustering:**
+ Segmentación: Se trata de dividir los registros en grupos heterogéneos y que son homogeneos dentro de los mismos. 
+ Segmentación por estilos de vida.

**Asociación:** 
+ Conocido también como Market Basket Analysis. Consiste en encontrar  registros que de forma natural se les puede encontrar juntos. Estos puedes ser productos, transacciones, secuencias de operaciones, etc. 

<img src="../src/img/ML_FIELDS.JPG" width="720" height="480"  align="center"/>

# 4. Áreas de apliación de Machine Learning

**Banca** 
+ Determinación de Fraude con el uso de Tarjetas de Crédito 
+ Generación de Score de Riesgos para clientes morosos.

**Retail** 
+ Análisis de Canasta 
+ Propensión a la compra de Productos Estrella

**Marketing** 
+ Targeting de acciones de Marketing 
+ Fidelización de Clientes

**Web** 
+ Optimización de Portales Web 
+ Redes sociales

### Ejemplos:
###  **a) Churn (Fuga de Clientes)**
**Objetivo:**
+ Modelos de retención de clientes y el Churn apuntan a mantener la lealtad y reducir la fuga.
+ La reducción de pérdida de clientes y la fidelización con estrategia de retención pueden ayudar significativamente al  crecimiento del negocio.

<img src="../src/img/Churn.JPG" width="360" height="360"  align="center"/>

## b) Segmentación de Valor / Comportamiento
**Objetivo** : 
+ Simplificar los grupos de clientes 
+  Perfilar los segmentos
+ Realizar una segmentación de valor (rentabilidad), seguido de una Segmentación Comportamental (uso TC y Débito) y vinculación con el Banco (Canales).

Incluye un análisis RFM

<img src="../src/img/SEGMENTACION.JPG" width="360" height="360"  align="center"/>

## c) Up Selling / Cross Selling

**Objetivo:** 
+ Ofrecer un producto de mayor valor. 
+ Generar una venta cruzada.
+ Modelar sobre los clientes que cuentan con el producto A, que no cuentan con el producto B.

El cliente Target con producto adecuado mejora la Fidelidad de los Clientes. Disminuye el Churn y aumenta los Ingresos.

<img src="../src/img/upcross.JPG" width="360" height="360"  align="center"/>

## 5. CONOCIMIENTOS Y CAPACIDADES DE UN DATA SCIENTIST
<img src="../src/img/DataScience.JPG" width="720" height="480"  align="center"/>

## 6.1. Metodología: Proceso KDD (Knowledge Discovery from Databases)

<img src="../src/img/KDD.JPG" width="720" height="480"  align="center"/>

## 6.2. Metodología: SEMMA (Sample, Explore, Modify, Model, Assess) 

<img src="../src/img/SEMMA.JPG" width="720" height="480"  align="center"/>

## 6.3. Metodología: CRISP – DM (Cross Industry Standard Process for DM)
### Objetivos:
+ Fomentar la interoperabilidad de las herramientas a través de todo el proceso de minería de datos
+ Eliminar la experiencia misteriosa y costosa de las tareas simples de minería de datos.

## Fases del proyecto de ciencia de datos:

<img src="../src/img/CrispDM.JPG" width="720" height="360"  align="center"/>


