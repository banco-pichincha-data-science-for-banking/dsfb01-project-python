# Predicción de clientes chalequeros (Telecomunicaciones)
<div style="text-align: justify">  
Existen diversas formas de reventas ilegales de telecolumicaciones, entre los principales tenemos a los proveedores informales de Internet y los denominados chalequeros. Dichas actividades no tienen ningún control de las autoridades y muchas veces son aprovechados por los delincuentes para realizar actividades ilegales. Para las empresas de telecomunicaciones genera perdidas de 4 millones de soles mensuales. El uso del servico móvil no se encuentra regulado y es aprovechado para su reventa. Además, estos chalequeros,realizan aberias en los teléfonos públicos con la finalidad de ser la única oferta.
</div>

<img src="../src/img/chalequeros.jpg" width="480" height="480" align="center">

# Interrogantes

Las interrogantes nos permitiran ir conociendo el negocio y dimensionar la problemática.

* ¿Cuáles son las operadores de telefonía que existen?
* ¿Qué tipo de llamadas existen?
* ¿Quienes son los clientes con servicio móvil, con algún plan pos pago o control?
* ¿Existe un grupo de clientes que revenden el servicio de llamadas?
* ¿Donde suelen estar?
* ¿Existen zonas de mayor concentración?

# Objetivo

Identificar a potenciales clientes que hacen uso indebido del servicio de telefonía móvil revendiendo ilegalmente la misma.

# Fuentes de información

Se cuentan con una base de datos sobre el historial 

![Estructura en BD](../src/img/estructura_bd.PNG)


# Tabla resumen

Se realizó un preprocesamiento previo para obtener la siguiente tabla resumen.

| Id | Campo                    | Descripción                                     | Tipo de Dato |
|----|--------------------------|-------------------------------------------------|--------------|
| 1  | PRODUCTO                 | Tipo de Contrato                                | varchar      |
| 2  | FA_TELF_ANO              | Año de alta                                     | int          |
| 3  | FA_TELF_MES              | Mes de alta                                     | int          |
| 4  | FA_TELF_DIA              | Dia de alta                                     | int          |
| 5  | FA_CLI_ANO               | Año de alta cliente                             | int          |
| 6  | FA_CLI_MES               | Mes de alta cliente                             | int          |
| 7  | FA_CLI_DIA               | Dia de alta cliente                             | int          |
| 8  | CODPLAN                  | Codigo de plan                                  | varchar      |
| 9  | DESPLAN                  | Descricion del plan                             | varchar      |
| 10 | DEPARTAMENTO             | Departamento                                    | varchar      |
| 11 | PROVINCIA                | Provincia                                       | varchar      |
| 12 | DISTRITO                 | Distrito                                        | varchar      |
| 13 | SMS_SAL                  | Nro de SMS                                      | float        |
| 14 | SAL_OFFNET_MIN           | Minutos Salientes Offnet                        | float        |
| 15 | SAL_OFFNET_NLLAM         | Nro. de llamadas Salientes Offnet               | float        |
| 16 | SAL_OFFNET_NDESTDIF      | Nro. de Destinos Salientes Offnet               | float        |
| 17 | SAL_FIJO_MIN             | Minutos Salientes Fijo                          | float        |
| 18 | SAL_FIJO_NLLAM           | Nro. de llamadas Salientes Fijo                 | float        |
| 19 | SAL_FIJO_NDESTDIF        | Nro. de Destinos Salientes Fijo                 | float        |
| 20 | SAL_ONNET_MIN            | Minutos Salientes Onnet                         | float        |
| 21 | SAL_ONNET_NLLAM          | Nro. de llamadas Salientes Onnet                | float        |
| 22 | SAL_ONNET_NDESTDIF       | Nro. de Destinos Salientes Onnet                | float        |
| 23 | SAL_ONNETRPB_MIN         | Minutos Salientes Rpb                           | float        |
| 24 | SAL_ONNETRPB_NLLAM       | Nro. de llamadas Salientes Rpb                  | float        |
| 25 | SAL_ONNETRPB_NDESTDIF    | Nro. de Destinos Salientes Rpb                  | float        |
| 26 | SAL_OTROS_MIN            | Minutos Salientes Otros                         | float        |
| 27 | SAL_OTROS_NLLAM          | Nro. de llamadas Salientes Otros                | float        |
| 28 | SAL_OTROS_NDESTDIF       | Nro. de Destinos Salientes Otros                | float        |
| 29 | SAL_TOTAL_NLLAM          | Total Nro. de llamadas Salientes                | float        |
| 30 | SAL_TOTAL_NDESTDIF       | Total Nro. de destinos Saliente                 | float        |
| 31 | ENT_TOTAL_MIN            | Entrante Total - Total de Minutos llamados      | float        |
| 32 | ENT_TOTAL_NLLAM          | Entrante Total - Nro Llamadas                   | float        |
| 33 | ENT_TOTAL_NDESTDIF       | Entrante Total - Número de Destinos Diferentes  | float        |
| 34 | SLMM_LLAM_ONNET_0_30     | Llamadas onnnet de 00_30 Seg                    | float        |
| 35 | SLMM_LLAM_ONNET_30_60    | Llamadas onnnet de 30_60 Seg                    | float        |
| 36 | SLMM_LLAM_ONNET_60_80    | Llamadas onnnet de 60_80 Seg                    | float        |
| 37 | SLMM_LLAM_ONNET_80_100   | Llamadas onnnet de 80_100 Seg                   | float        |
| 38 | SLMM_LLAM_ONNET_100      | Llamadas onnnet mas de 100 Seg                  | float        |
| 39 | ESTANEXO                 | Estado anexo                                    | varchar      |
| 40 | SAL_TOTAL_MIN            | Total Min Salientes                             | float        |
| 41 | SLMM_P_LLAM_ONNET_0_30   | Porcentaje de llamadas onnet entre 0 a 30 seg   | float        |
| 42 | SLMM_P_LLAM_ONNET_30_60  | Porcentaje de llamadas onnet entre 30 a 60 seg  | float        |
| 43 | SLMM_P_LLAM_ONNET_60_80  | Porcentaje de llamadas onnet entre 60 a 80 seg  | float        |
| 44 | SLMM_P_LLAM_ONNET_80_100 | Porcentaje de llamadas onnet entre 80 a 100 seg | float        |
| 45 | SLMM_P_LLAM_ONNET_100    | Porcentaje de llamadas onnet más 100 seg        | float        |
| 46 | TARGET                   | Variable Objetivo                               | int          |
| 47 | FACT_0109                | Facturación de Enero 19                         | float        |
| 48 | FACT_0209                | Facturación de Febrero 19                       | float        |
| 49 | ANT_LINEA                | Antigüedad Linea (meses)                        | int          |
| 50 | ANT_CLIENTE              | Antigüedad Cliente (meses)                      | int          |

 ## IMPORTANDO LIBRERIAS
 ```
 import pandas as pd
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as sc
```
### Variables categoricas
1. **En clientes**: 

ID_PRODUCTO, ID_FECHA_ALTA_TELFONO, ID_FECHA_ALTA_CFLIENTE, ID_UBIGEO, ID_SALIDAS

2. **En producto**:
ID_PRODUCTO, POSTPAGO, CODPLAN, DESPLAN
3. **En Fechas**: 
ID_FECHA
4. **EN UBIGEO**:
ID_UBIGEO, DEPARTAMENTO, PROVINCIA, DISTRITO
5. **Salidas**:
ID_SALIDAS
```
tipo_datos_clientes = {
    'ID_PRODUCTO':'category','ID_FECHA_ALTA_TELFONO':'category','ID_FECHA_ALTA_CFLIENTE':'category','ID_UBIGEO':'category'
    ,'ID_SALIDAS':'category'
}

tipo_datos_producto = {
    'ID_PRODUCTO':'category','POSTPAGO':'category','CODPLAN':'category','DESPLAN':'category'
}

tipo_datos_fechas = {
    'ID_FECHA':'category'
}

tipo_datos_ubigeo = {
    'ID_UBIGEO':'category','DEPARTAMENTO':'category','PROVINCIA':'category','DISTRITO':'category'
}

tipo_datos_salidas = {
    'ID_SALIDAS':'category'
}
```
## DEFINIENDO RUTA
```
ruta='D:/Proyectos/Congreso/Data'
clientes='00.FACT_fraudemovilBEBOOO.csv'
producto='01.DIM Producto.csv'
fechas='02.DIM Fechas.csv'
ubigeo='03.DIM Ubigeo.csv'
salidas='04.DIM Salidas.csv'
path0=os.path.join(ruta,clientes)
path1=os.path.join(ruta,producto)
path2=os.path.join(ruta,fechas)
path3=os.path.join(ruta,ubigeo)
path4=os.path.join(ruta,salidas)
```
## LEYENDO BASES DE DATOS DISPONIBLES
```
bd_clientes=pd.read_csv(path0,sep=';',dtype=tipo_datos_clientes)
bd_producto=pd.read_csv(path1,sep=';',dtype=tipo_datos_producto)
bd_fechas=pd.read_csv(path2,sep=';',dtype=tipo_datos_fechas)
bd_ubigeo=pd.read_csv(path3,sep=';',dtype=tipo_datos_ubigeo)
bd_salidas=pd.read_csv(path4,sep=';',dtype=tipo_datos_salidas)
```
### Explorando las bases de datos
### a) Clientes

Eliminando Duplicados
```
bd_clientes=bd_clientes.drop(columns=['ID'])
bd_clientes=bd_clientes.drop_duplicates()
bd_clientes.head()
bd_clientes.shape
bd_clientes.info()
```
### b) Producto:
```
bd_producto.head()
bd_producto.shape
bd_producto.info()
```
### c) Fechas
```
bd_fechas.head()
bd_fechas.shape
bd_fechas.info()
```
### d) Ubigeo
```
bd_ubigeo.head()
bd_ubigeo.shape
bd_ubigeo.info()
```
### e)Salidas 
```
bd_salidas.head()
bd_salidas.shape
bd_salidas.info()
```

# Union de las bases de datos
<img src="../src/img/pandas_joins.jpg" width="480" height="480" align='center'>

## Uniendo  con PRODUCTO
```
bd_fraude = pd.merge(left=bd_clientes,right=bd_producto,how='left',left_on='ID_PRODUCTO', right_on='ID_PRODUCTO')
                       
bd_fraude.head()
bd_fraude.shape
```
## Uniendo con FECHA DE ALTA TELÉFONO
```
bd_fraude = pd.merge(left=bd_fraude,right=bd_fechas, how='left',left_on='ID_FECHA_ALTA_TELFONO',right_on='ID_FECHA')
bd_fraude.head()
```
```
# renombrando la fecha de alta del teléfono

bd_fraude=bd_fraude.rename(columns = {'FECHA':'FECHA_ALTA_TELEFONO'}) 
bd_fraude.drop(['ID_FECHA'], axis=1,inplace=True) # elimino la columna porque se repite
bd_fraude.head()
## Uniendo con FECHA DE ALTA CLIENTE
bd_fraude = pd.merge(left=bd_fraude,right=bd_fechas, how='left', 
left_on='ID_FECHA_ALTA_CFLIENTE',right_on='ID_FECHA')
bd_fraude.head()
```
```
# renombrando la fecha de alta del CLIENTE

bd_fraude=bd_fraude.rename(columns = {'FECHA':'FECHA_ALTA_CLIENTE'}) 
bd_fraude.drop(['ID_FECHA'], axis=1,inplace=True)
bd_fraude.head()
bd_fraude.shape
```
## Uniendo con UBIGEO
```
bd_fraude = pd.merge(left=bd_fraude,right=bd_ubigeo, how='left', 
left_on='ID_UBIGEO',right_on='ID_UBIGEO')
bd_fraude.head()
bd_fraude.shape
```
## Uniendo con SALIDAS
```
bd_fraude = pd.merge(left=bd_fraude,right=bd_salidas, how='left', 
left_on='ID_SALIDAS', right_on='ID_SALIDAS')
bd_fraude.head()
bd_fraude.shape
```
## Obteniendo la BASE DE DATOS FINAL
```
bd_telcofraude=bd_fraude
bd_telcofraude.head(10)
```
```
# Recordamos que las variables: ID_FECHA_ALTA_TELFONO,ID_FECHA_ALTA_CFLIENTE solo sirven para 
# determinar las variables FECHA_ALTA_TELEFONO ,FECHA_ALTA_CLIENTE, por tanto se procede a eliminarlas
bd_telcofraude.drop(columns=['ID_FECHA_ALTA_TELFONO','ID_FECHA_ALTA_CFLIENTE'],inplace=True)
# Cambiando las variables fecha 
bd_telcofraude['FA_TELF_ANO'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_TELEFONO']).year
bd_telcofraude['FA_TELF_MES'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_TELEFONO']).month
bd_telcofraude['FA_TELF_DIA'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_TELEFONO']).day

bd_telcofraude['FA_CLI_ANO'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_CLIENTE']).year
bd_telcofraude['FA_CLI_MES'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_CLIENTE']).month
bd_telcofraude['FA_CLI_DIA'] = pd.DatetimeIndex(bd_telcofraude['FECHA_ALTA_CLIENTE']).day

bd_telcofraude.drop(columns=['FECHA_ALTA_TELEFONO','FECHA_ALTA_CLIENTE'],inplace=True)
```
```
# Generando las vairables:

table_final_nllam=bd_telcofraude[["SAL_OFFNET_NLLAM","SAL_FIJO_NLLAM","SAL_ONNET_NLLAM","SAL_ONNETRPB_NLLAM","SAL_OTROS_NLLAM"]]
table_final_ndestdif=bd_telcofraude[["SAL_OFFNET_NDESTDIF","SAL_FIJO_NDESTDIF","SAL_ONNET_NDESTDIF","SAL_ONNETRPB_NDESTDIF","SAL_OTROS_NDESTDIF"]]
bd_telcofraude["SAL_TOTAL_NLLAM"]=table_final_nllam.sum(axis=1,skipna=True)
bd_telcofraude["SAL_TOTAL_NDESTDIF"]=table_final_ndestdif.sum(axis=1,skipna=True)

bd_telcofraude.head(15)
```
```
def funcion_conoc_data(dataframe):
    df_dtype=pd.DataFrame(dataframe.dtypes,columns=['Tipo Dato'])
    df_dtype=df_dtype.reset_index().rename(columns={'index':'Variable'})
    df_na=pd.DataFrame(100*dataframe.isna().sum()/bd_clientes.shape[0],columns=['Porcentaje NA'])
    df_na=df_na.reset_index().rename(columns={'index':'Variable'})
    df_unic=pd.DataFrame()
    variables=list()
    unicos=list()
    for i in dataframe.columns:
        variables.append(i)
        unicos.append(len(dataframe[i].unique()))
    df_unic['Variable']=variables
    df_unic['Cantidad Valores Unicos']=unicos
    df_all=df_dtype.merge(df_na,how='left', on ='Variable')
    df_all=df_all.merge(df_unic,how='left', on ='Variable')
    return df_all
```
```
    funcion_conoc_data(bd_telcofraude)
