# Python

> Es un lenguaje de programación interpretado orientado a objetos de proposito general que te permite trabajar rápidamente e integrar sistemas de manera más efectiva. 

![Tendencia Python](https://zgab33vy595fw5zq-zippykid.netdna-ssl.com/wp-content/uploads/2017/09/projections-1-1024x878.png)

Algunas de las [características notables](https://wiki.python.org/moin/BeginnersGuide/Overview) de Python:

* Utiliza una sintaxis elegante, que facilita la lectura de los programas que escribe.
* Es un lenguaje fácil de usar que simplifica el funcionamiento de su programa. Esto hace que Python sea ideal para el desarrollo de prototipos y otras tareas de programación ad-hoc, sin comprometer la capacidad de mantenimiento.
* Viene con una gran biblioteca estándar que admite muchas tareas de programación comunes, como conectarse a servidores web, buscar texto con expresiones regulares, leer y modificar archivos.
* El modo interactivo de Python facilita la prueba de fragmentos cortos de código. También hay un entorno de desarrollo incluido llamado IDLE.
* Se extiende fácilmente agregando nuevos módulos implementados en un lenguaje compilado como C o C ++.
* También se puede incrustar en una aplicación para proporcionar una interfaz programable.
* Funciona en cualquier lugar, incluyendo Mac OS X , Windows , Linux y Unix , con versiones no oficiales también disponibles para Android y iOS.
* Es software libre en dos sentidos. No cuesta nada descargar o usar Python, o incluirlo en su aplicación. Python también se puede modificar y redistribuir libremente, porque mientras el idioma tiene derechos de autor, está disponible bajo una licencia de código abierto .

Algunas características del lenguaje de programación de Python son:

* Hay disponibles una variedad de tipos de datos básicos: números (coma flotante, enteros complejos y enteros largos de longitud ilimitada), cadenas (tanto ASCII como Unicode), listas y diccionarios.
* Python admite la programación orientada a objetos con clases y herencia múltiple.
* El código se puede agrupar en módulos y paquetes.
* El lenguaje es compatible con generar y capturar excepciones, lo que resulta en un manejo de errores más limpio.
* Los tipos de datos son fuertemente y dinámicamente tipados. La mezcla de tipos incompatibles (por ejemplo, intentar agregar una cadena y un número) hace que se genere una excepción, por lo que los errores se detectan antes.
* Python contiene funciones de programación avanzadas, como generadores y listas de comprensión.
* La administración automática de memoria de Python lo libera de tener que asignar manualmente y liberar memoria en su código.


Existen más de [100 editores de Python](https://wiki.python.org/moin/PythonEditors), que pueden ser multiplataforma, sólo para Unix, windows, Mac, en línea, para móviles, etc. En la página oficial, pueden encontrar una colección de direcciones web con acceso a [libros para el desarrollo con Python](https://wiki.python.org/moin/PythonBooks) gratuitos y de paga.

En conclusión: Python es un lenguaje de alto nivel, dinámico, proposito general

# Típicos programas

* CL/s (Líneas de comando)
* GUI desktop apps
* Webs backend applications
* Robots and harware
* Scientific apps

[Ejemplo de aplicaciones](https://www.escuelapython.com/grandes-proyectos-hechos-python/): 
Instagram, Pinterest, Dropbox, Battlefield 2, BitTorrent, Ubuntu Software Center, Google App Engine, NASA, reconocimiento facial con raspberry pi, generar la primera foto de un agujero negro.

![Katie Bouman](https://periodicocorreo.com.mx/wp-content/uploads/2019/04/Katie.jpg)

## Python en Inteligencia Artificial (IA)

Librerias como **Scikitl**, **Keras** y **TensorFlow** contienen mucha información sobre las funcionalidades del aprendizaje automático.

Ejemplos de aplicaciones de la IA:

* NLP: [Better Language Models](https://openai.com/blog/better-language-models/#sample8)
* Arte: [Few shot learning](https://arxiv.org/pdf/1905.08233v1.pdf)
* Game: [AlphaStar](https://deepmind.com/blog/alphastar-mastering-real-time-strategy-game-starcraft-ii/) 
* Música: [MuseNet](https://openai.com/blog/musenet/)
* Robot: [Unifying Physics and Deep Learning with TossingBot](https://ai.googleblog.com/2019/03/unifying-physics-and-deep-learning-with.html)
* Más información: [YouTube DotCSV](https://www.youtube.com/channel/UCy5znSnfMsDwaLlROnZ7Qbg)

## Python en Big Data
Python cuenta con bibliotecas de procesamiento de datos como **Pydoop**, **PySpark** y **Dask**, facilitan aún más el análisis y la gestión de datos:

Ejemplos de Big Data con Python:

* [Netflix: Using Big Data to Drive Big Engagement](https://static1.squarespace.com/static/5671cd2ddf40f3fd5f4a05f2/t/57d898abb3db2b9a71636d48/1473812655921/Netflix+Teradata.pdf)
* [SUNAT uso indebido de comprobantes de pago](https://gestion.pe/tu-dinero/sunat-pone-marcha-sistema-alertas-evitar-indebido-comprobantes-pago-245732)
* [SUNAT Evasión fiscal](https://blogs.iadb.org/gestion-fiscal/es/utilizando-big-data-para-combatir-la-evasion/)

## Python en Data Science
Los motores numéricos de Python como **Pandas** y **NumPy** son los favoritos de los investigadores. Además, Python se ocupa de los datos tabulares, matriciales y estadísticos, e incluso los visualiza con bibliotecas populares como **Matplotlib** y **Seaborn**.

* [Hipercrecimiento de Airbnb](https://venturebeat.com/2015/06/30/how-we-scaled-data-science-to-all-sides-of-airbnb-over-5-years-of-hypergrowth/)
* [Spotify listas de reproducción](https://towardsdatascience.com/spotifys-this-is-playlists-the-ultimate-song-analysis-for-50-mainstream-artists-c569e41f8118)
* [Clear explanations of machine learning](https://distill.pub/)

## Bonus
La tendencia para este año!! 
[Developer Survey Results
2019](https://insights.stackoverflow.com/survey/2019#technology)

# Data Science for Banking

[Learn Data Science in 3 Months](https://www.youtube.com/watch?v=9rDhY1P3YLA)

Las bases, tipos de datos, variables, bucles, condiciones, modulos, sintaxis más básicas, gramática y como utilizar sus reglas para utilizar sus funciones.

## Variables y operadores
Se asigna un valor a un objeto:
```python
X=3
Y=5
```
Para imprimir en consola se utiliza print(). A partir de la versión 3.x
```python
print(X)

Edad_actual=48
Personaje = 'Elon Musk'
Empresas = 'PayPal, Tesla Motors, SpaceX, Hyperloop, SolarCity, The Boring Company, Neuralink y OpenAI'
Fortuna_estimada = 20.8
Fecha_Nacimiento = 19710628 #aaaammdd

print(f'La edad actual de {Personaje} es {Edad_actual}. Cofundador de {Empresas} y tiene una fortuna estimada de {Fortuna_estimada} mil millones USD')
```
Conocer el tipo de dato que almacena el objeto con type()

```python
type(Edad)
type(Empresas)
type(Fortuna_estimada)
```
Funcionan todos los operadores aritmeticos (+,-,*,/,%)
```python
Edad_inicio = 24
Crecimiento_anual_fortuna = Fortuna_estimada/(Edad_actual-Edad_inicio)

Día_nacimiento = Fecha_Nacimiento%100

print(f'El crecimiento promedio anual de su fortuna es de {Crecimiento_anual_fortuna} mil millones USD. El día de su nacimiento fue {Día_nacimiento}')
```
## Instrucciones de control

### Condicional
```python
if (Edad_inicio%2==0) :
    print(f'{Edad_inicio} es par')
else : 
    print(f'{Edad_inicio} es impar')
```

### Repetitivas
1. Número de repeticiones conocido
```python
n=0
for i in range(1,10,2):
    print(i)
    n+=1 #n=n+1 : Contador
```

2. Número de repeticiones desconocidos
```python
anio=2001
while anio<2012:
    print(f'Creación de la empresa en el año: {anio}')
    anio+=2 # anio=anio+2 : Acumulador
```

## Funciones

1. Función que determina el máximo entre dos números
```python
def max2numeros(a,b):
    m=(abs(a+b)+abs(a-b))/2
    print(f'El máximo número entre {a}, {b} es {m}')
    return(m)
    
x=max2numeros(8,5)
```

2. Función para calcular el factorial de un número
```python
def factorial(n):
    if (n>1):
        return(n*factorial(n-1))
    else:
        return(1)

factorial(3)
```

## Arreglos
Conjunto de datos dispuestos en filas y columnas

### [Arreglos lineales](https://python-para-impacientes.blogspot.com/2014/01/cadenas-listas-tuplas-diccionarios-y.html)
Las cadenas, listas y tuplas son distintos tipos de secuencias. Una secuencia es un tipo de objeto que almacena datos y que permite el acceso a una parte determinada de su información utilizando índices. 

Las listas, tuplas, diccionarios y conjuntos (set) son estructuras que permiten trabajar con colecciones de datos. El primer elemento de una lista o de una tupla ocupa la posición 0.
```python
notas=[20,12,14,12,18,5]    #Lista
a = (1, 2, 3)               #Tuplas (inmutables)
b = set(notas)              #Sets (conjunto: no se repite los elementos)
c = {'x': 1, 'y': 2, 'z': 3}#Diccionarios: "Key:Value"
```

Métodos asociados a las listas:
```python
len(notas)
notas.append(17)
notas.extend(a)
notas.index(12) # 1, los indices comienzan en CERO
notas.insert(3,13)
notas.count(20)
notas.sort() #notas.reverse() 
```

Recorrido de un array

```python
for n in notas: print(n)
for k, v in c.items(): print(k + "=>" + str(v))
```

### Matrices

```python
A = [
     ['Roy',80,75,85,90,95],
     ['John',75,80,75,85,100],
     ['Dave',80,80,80,90,95]
     ]
print(A[0])
print(A[0][1])
```

Manejo sofisticado de las estructuras de datos

```python
import numpy as np
notas=np.array([20,12,14,15,18,5])
print(notas)
type(notas)     #numpy.ndarray
notas.dtype     #Tipo de Array
notas.size      #Tamaño del arreglo
notas.sum()
notas.max()
notas.min()
notas.mean()

notas[3]        #Leer un dato
notas[[2,4,5]]  #Leer varios Datos

notas[1]=17    #Reemplazar
```

Recorrido de un array

```python
i=0
for x in notas:
  i=i+1
  print("Elemento : " + str(i) + ": " + str(x))
```
Autogeneración
```python
a=np.repeat(4,3)        #Repeticiones 4 4 4
b=np.arange(2,15,3)     #Secuencial 2  5  8 11 14
```
Matrices
```python
A=np.array([10,11,12,13,14,15]).reshape(3, 2)   #Por fila

B=np.array([10,11,12,13,14,15]).reshape(3, 2, order='F')    #Por Columna

B
B[2,0]      #Leer 1 dato (3ra fila,1ra columna)
B[1,:]      #Leer toda una fila
B[:,0]      #Leer toda una columna
B[[1,2],1]  #Leer la Fila 1 y 2 de la columna 1 

A.shape
A.transpose()
```
## Integración y Transformación de Datos

### Importación de librerias

```python
import pandas as pd
import numpy as np
```

Pandas ofrece un sin número de funciones que permiten manipular datos. 
Como ejemplo usaremos una data de Kaggle sobre [New York City current job postings](https://www.kaggle.com/new-york-city/new-york-city-current-job-postings)

Una estructura importante de la analítica son los **DataFrame**. Es una estructura de datos tabular de dos dimensiones, mutable, potencialmente heterogénea con ejes etiquetados (filas y columnas). Las operaciones aritméticas se alinean en ambas etiquetas de fila y columna. Puede considerarse como un contenedor tipo lista para objetos de una serie. La principal estructura de datos en pandas.

> Nota: También se pueden crear DataFrame en Spark

### Importación local de tablas estructuradas
```python
path = '../src/db/nyc-jobs.csv'
df = pd.read_csv(path)

pd.read_csv?
```

Métodos asociados a un DataFrame de pandas
```python
df.head()           #Visualizar los primeros 5 registros
df.columns.values   #Lista de variables/atributos
df.describe()       #Auditoria de la tabla
```

### Importación en otros formatos

1. sav,sas
```python
import savReaderWriter as spss  #pip install savReaderWriter

path="D:/BDA_VIII/DataSets/vinos.sav"
with spss.SavReaderNp (path) as reader:
    records = reader.all()

ds = pd.DataFrame(records)
ds.head()
```
2. json
```python
```

3. web scraping
```python
```

### Selección y filtros

```python
df.iloc[:, [2]].values          # 1 Variable
df.Agency
df.iloc[:, [2, 3]].values       # ds1[c(1,4)] Varias cols
df[['Posting Type','Level','Process Date']]
df.filter(items=['Posting Type', 'Level'])
df.filter(regex='Salary*', axis=1)     # Selecciona las columnas por una expresión regular

df.ix[1]                    # 1 Observación por indice
df.ix[[4,11,33]]            # n Observaciones por indice

df[df['Salary Range From'] >=40000]       # filtro lógico
df[~(((df['Posting Type']=='Internal') & (df['Salary Range From'] >=40000)) | ((df.Agency=='DEPARTMENT OF BUSINESS SERV.') & (df.Ancho<100)))]

"SELECT Ancho, Sexo, Altura FROM ds1 WHERE NOT (Sexo==1 & Altura<60)
df1=df[['Ancho','Sexo','Altura']][~((df.Sexo==1) & (df.Altura<60))]
```

### Integración
Join
```python
caller = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3', 'K4', 'K5'],
                        'A': ['A0', 'A1', 'A2', 'A3', 'A4', 'A5']})

other = pd.DataFrame({'key': ['K0', 'K1', 'K2'],
                       'B': ['B0', 'B1', 'B2']})

x=caller.join(other, lsuffix='_caller', rsuffix='_other')
y=caller.join(other.set_index('key'), on='key')
```
