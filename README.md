# DSFB01 Project Python

Proyecto de ciencia de datos con Python.

**Nota**

> Todo archivo contenido en los presentes repositorios es considerado por los autores como publico. Dichos archivos son necesarios para el desarrollo del taller de Machine Learning con Python presentado 
en el congreso UCDS y patrocinado por el **Banco Pichincha**


## Contenido

* En la carpeta [code](/code) se encuentran los scripts de Python y Jupyter Notebook.
* En [document](/document) se almacenan los documentos relacionados a la elaboración del proyecto. Estos incluyen los  temas sobre metodología de proyectos de ciencia de datos (CRISP-DM, Microsoft, entre otros.).
* En [result](/result) se guardan todos los resultados obtenidos en el desarrollo del proyecto.
* Por último, en [src](/src) se almacena todos las fuentes utilizadas en el proyecto, incluye archivos multimedia (imagen, videos, audio), base de datos, tablas, fuentes de información, entre otros.